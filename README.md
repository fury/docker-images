# side linux docker images
    
### how to build image
    
- ```sudo service docker start```
- ```git clone https://codeberg.org/side/docker-images.git```
- ```mkdir -p ../docker/soot```
- ```cp Dockerfile gentar.sh docker/```
- ```cd ../docker```
- ```sudo ./gentar.sh```
- ```sudo docker build --tag tagname .```
- ```sudo docker images```

    
Thanks to Sulin project https://gitlab.com/sulinos/devel/SulinDocker/


### init

- ```sudo docker service start```
- ```sudo docker run -v %host_source_repo_dir%:%docker_mount_point% -v %host_output_dir%:%docker_mount_point% -v %host_tarball_dir%:%docker_mount_point% -v %host_dep_cache_dir%:%docker_mount_point% -itd --security-opt=seccomp:unconfined %tagname%```
- ```sudo docker ps -a```
- ```sudo docker start id_new_container```
- ```sudo docker attach id_new_container```
- ```service dbus start```
- ```pisi rdb -y```


### build package

- ```service dbus start```
- ```pisi hs -l %random_number%``` check initial state
- ```pisi bi --output-dir %% -vdy --ignore-safety %path_to_pspec.xml%```
- ```pisi hs -t %initial_state% -y```


### stopping

- ```service dbus stop```
- ```exit```
- ```sudo service docker stop```
