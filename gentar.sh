#!/bin/bash
set +h
umask 022

pisi-cli ar side https://sourceforge.net/projects/lxdeside/files/side/side-devel/pisi-index.xml.xz -Dsoot --yes-all

pisi-cli it acl \
            attr \
            autoconf \
            automake \
            bash \
            baselayout \
            bc \
            binutils \
            bison \
            bzip2 \
            catbox \
            check \
            comar \
            comar-api \
            coreutils \
            curl \
            dbus \
            dbus-python \
            diffutils \
            e2fsprogs \
            elfutils \
            expat \
            file \
            findutils \
            flex \
            gawk \
            gcc \
            libgcc \
            gdbm \
            gettext \
            glibc \
            gmp \
            gnuconfig \
            gperf \
            grep \
            gzip \
            iana-etc \
            intltool \
            kbd \
            kernel-headers \
            kmod \
            lf \
            less \
            leveldb \
            libcap \
            libffi \
            libidn2 \
            libpsl \
            mpc \
            libnghttp2 \
            libpcre2 \
            libpipeline \
            libssh2 \
            libtasn1 \
            libtool \
            libunistring \
            libunwind \
            libX11 \
            libXau \
            libxcb \
            libxml2 \
            libxcrypt \
            lzip \
            m4 \
            make \
            make-ca \
            mpdecimal \
            mpfr \
            mudur \
            nano \
            ncurses \
            openssl \
            p11-kit \
            pardus-python \
            patch \
            perl \
            perl-XML-Parser \
            piksemel \
            pisi \
            pkgconf \
            plyvel \
            procps \
            psmisc \
            pycurl \
            python \
            python-magic \
            python3 \
            python3-devel \
            python-psutil \
            readline \
            sed \
            snappy \
            sqlite \
            start-stop-daemon \
            tar \
            texinfo \
            urlgrabber \
            util-linux \
            which \
            xtrans \
            xz \
            zlib \
            zstd --ignore-safety --ignore-comar -Dsoot --yes-all

pisi-cli rr side -Dsoot

make-ca -Dsoot -g

pisi-cli ar devel https://sourceforge.net/projects/lxdeside/files/side/side-devel/pisi-index.xml.xz -Dsoot --yes-all
pisi-cli ar side https://codeberg.org/side/side.git
pisi-cli ar bsides https://codeberg.org/uglyside/bsides.git

pisi-cli dc -Dsoot
pisi-cli rdb -Dsoot --yes-all

rm -rf soot/usr/share/man/*
rm -rf soot/usr/share/info/*
rm -rf soot/usr/share/copyright/*
rm -rf soot/usr/share/doc/*
rm -rf soot/usr/share/locale/*

cp soot/usr/share/etc/* soot/etc

cd soot
tar cf ../img.tar .
cd ..
