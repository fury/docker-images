# syntax=docker/dockerfile:1
FROM scratch

ADD img.tar /

RUN /usr/sbin/make-ca -C /etc/ssl/certdata.txt
RUN /usr/bin/python3 -m ensurepip --default-pip
ENV EDITOR=/usr/bin/nano
WORKDIR /root

CMD ["/usr/bin/bash"]
